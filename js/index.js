
requirejs.config({
    baseUrl: 'js',
    paths: {
        api: 'js/api/index.js'
    }
});

var profile = skills = education = working = null;

/**
 * @async
 * @param {Object.<string, function>} api
 * @returns {Promise<void>} void
 */
async function call(api) {
  await Promise.all([
    api.getProfile().then(result => result.json()),
    api.getSkills().then(result => result.json()),
    api.getEducation().then(result => result.json()),
    api.getWorking().then(result => result.json()),
  ]).then(json => {
    [profile, skills, education, working] = json;
  });
};

/**
 * @returns {void} void
 */
function writeProfile() {
  const nameElems        = document.querySelectorAll(".write_full_name");
  const jobElems         = document.querySelector(".profile__job");
  const emailElems       = document.querySelector("#write_email");
  const descriptionElems = document.querySelector("#write_description");
  const birthdateElems   = document.querySelector("#write_birthdate");
  const optionsDate      = {year: 'numeric', month: 'long', day: 'numeric'};

  profile.data.forEach(d => {
    nameElems.forEach(elem => {
      elem.innerHTML += d.name;
    });
    
    jobElems.innerHTML         += d.jobTitle;
    emailElems.innerHTML       += d.email;
    descriptionElems.innerHTML += d.description;
    birthdateElems.innerHTML   += (new Date(d.birthday.date)).toLocaleDateString(undefined, optionsDate);
  });
}

/**
 * @param {Object.<string, string|number>} skill
 * @returns {string} string
 */
function skillsDispatcher(skill) {
  return `<div class="skill__unit" title="${skill.percent}%"><h3>${skill.name}</h3><div class="skill__progress w-100"><div class="skill__percentage w-${skill.percent}"></div></div></div>`;
}

/**
 * @param {Object.<string, string>} history
 * @returns {string} string
 */
function historyDispatcher(history) {
  return `<div class="history__unit"><div class="history__header d-flex f-column"><span class="history__title">${history.jobTitle}</span><span class="history__date">${history.from} - ${history.to}</span></div><p>${history.description}</p></div>`;
}

/**
 * @param {Object.<string, string>} infos
 * @param {string} selector
 * @param {string} funcName
 * @returns {void} void
 */
function writeHtml(infos, selector, funcName) {
  const elem = document.querySelector(selector);

  const all = [];
  infos.data.forEach(d => {
    const one = window[funcName](d);
    all.push(one);
  });

  elem.innerHTML = all.join("");
}

/**
 * @returns {void} void
 */
function turnOnContent() {
  const elem = document.querySelector("#wrapper");
  elem.classList.remove("d-none");
  elem.classList.add("d-flex");
}

/**
 * @async
 * @returns {Promise<void>} void
 */
async function main(api) {
  await call(api);
  writeProfile();
  writeHtml(skills, ".skills__container", "skillsDispatcher");
  writeHtml(working, ".write_working-history", "historyDispatcher");
  writeHtml(education, ".write_education-history", "historyDispatcher");
  turnOnContent();
}

// Start the main app logic.
requirejs(["js/api/index.js"], function (api) {
  main(api);
});

