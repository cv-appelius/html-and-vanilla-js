define(function() {
  const http = {
    get: (url) => {
      return template(url, "GET");
    }
  };

  return http;
});

/**
 * @param {string} url
 * @param {string} method
 * @returns {Promise<Response>} Response
 */
function template(url, method) {
  const baseURL = "https://api.appelius-theo.fr";

  return fetch(baseURL+url, {
    method,
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
  });
}
