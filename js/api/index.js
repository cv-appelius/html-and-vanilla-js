define(["./../service/http.js"], function(http) {
  const api = {
    getProfile: () => {
      return http.get("/profile/");
    },
    getSkills: () => {
      return http.get("/skills/");
    },
    getEducation: () => {
      return http.get("/history/education");
    },
    getWorking: () => {
      return http.get("/history/working");
    },
  };

  return api;
});
